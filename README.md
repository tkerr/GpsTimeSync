# GpsTimeSync 
AB3GY GPS time sync application intended for amateur radio portable operation.

Used to synchronize computer time to UTC for portable operation when an 
internet connection is not available.

Consists of an Arduino-based GPS module that communicates with a host 
computer over a serial port.  The host computer runs a Python script 
that periodically requests time from the GPS module and updates its 
system time accordingly.

The GPS Time Sync module provides a SCPI-like protocol interface for command 
and control. (SCPI = Standard Commands for Programmable Instruments, a popular 
standard for test equipment programming.)    
Example command: `:GPS:TIME?`

The Python script used to synchronize computer time runs under Windows and 
Linux, but requires elevated privilege in order to set system time. 
In Windows, this means running as administrator. In Linux, this means running 
with sudo.  A batch file (Windows) and a shell script (Linux) are provided to 
facilitate running with elevated privilege.

### Hardware 
The AB3GY GPS Time Sync module is based on the following hardware:
  - Adafruit Feather M0 board - based on Microchip ATSAMD21 Cortex M0
  - Adafruit Ultimate GPS module - based on MediaTek MTK3339 chipset

The hardware module reads the GPS time and maintains synchronization to the 
GPS PPS (Pulse Per Second) signal.  If the GPS or PPS signal is lost, then a 
32.768KHz oscillator keeps time until a GPS fix is re-acquired.

### IDE 
  - Arduino IDE 1.8.12+
  
### Dependencies 
GpsBasic library: https://gitlab.com/tkerr/GpsBasic   
ISerial library: https://gitlab.com/tkerr/ISerial   
TickZero library: https://gitlab.com/tkerr/TickZero   

### Author
Tom Kerr AB3GY
ab3gy@arrl.net

### License
Released under the MIT License
https://opensource.org/licenses/MIT

### How To Use
1. Connect the AB3GY GPS Time Sync module to the host computer using a USB 
cable.  The host OS should create a virtual serial port.

2. Modify the batch or shell script to change directory to the one containing
the `GpsTimeSync.py`, `GpsTimeSync.bat` and `GpsTimeSync.sh` files.

3. Modify the batch or shell script to use the correct virtual serial port.

4. Run the script with elevated privilege:
   - In Windows, right click on `GpsTimeSync.bat` and select **Run as administrator**
   - In Linux, open a terminal window and type `sudo GpsTimeSync.sh`

### GPS Time Sync Module Commands
<pre>

    *CLS - Clear the parser
           Response: 800 (OPC - OPeration Complete)

    *IDN? - Device identification
            Response: AB3GY,GpsTimeSync,0,&lt;Version>
            Corresponds to the following: &lt;Manufacturer>,&lt;Model>,&lt;Serial>,&lt;Version>

    *OPC - Generates the Operation Complete message
           Response: 800 (OPC - OPeration Complete)
        
    *OPC? - Same as OPC

    *RST - Device reset
           Response: 800 (OPC - OPeration Complete)

    :GPS
        :CMD &lt;cmd> - Send a command to the GPS module
                        Response: 800 (OPC - OPeration Complete) if successful, 
                        -200 (Command execution error) if GPS command failed to send
        :COOR? - Query GPS coordinates
                 Response: &lt;lat D.dddddd>,&lt;lon DD.dddddd>,&lt;alt M.mmmmmm>,&lt;status>
        :HEAD? - Query GPS heading
                 Response: &lt;speed (knots)>,&lt;track (deg)>,&lt;magnetic variation (deg)>,&lt;status>
        :QUAL? - Query GPS data quality
                 Response: &lt;num satellites in view>,&lt;fix quality>,&lt;HDOP (m)>,&lt;status>
                 Fix quality: 0 = Invalid, 1 = GPS fix, 2 = DGPS fix
                 HDOP = Horizontal Dilution of Precision
        :TIME? - Query GPS time
                 Response: &lt;year>,&lt;month>,&lt;day>,&lt;hour>,&lt;minute>,&lt;second>,&lt;millisecond>,&lt;status>

    :LIST - List all GPS Time Sync module commands
            Respone: Hierarchical list of all commands

    :STAT
        :COND? - Query GPS status
                 Response: -400 or one of the 4xx codes (see below)

    :SYST
        :ECHO 0|1 - Turn command echo off/on (0 = off, 1 = on)
                    Response: 800 (OPC - OPeration Complete)
        :LEDS 0|1 - Turn LEDs off/on (0 = off, 1 = on)
                    Response: 800 (OPC - OPeration Complete)
        :NMEA 0|1 - Turn GPS NMEA sentence echo off/on (0 = off, 1 = on)
                    Response: 800 (OPC - OPeration Complete)
</pre>

### GPS Time Sync Module Status
The following codes are returned by the above commands in the &lt;status> field:
 - Normal operation:
   - 401 = GPS fix but no time sync to PPS or RTC
   - 402 = No GPS fix but time sync to PPS (transitional case)
   - 403 = GPS fix with time sync to PPS (primary operational case)
   - 404 = No GPS fix, using time sync to RTC (fallback operational case)
   - 405 = GPS fix, time still sync'd to RTC (fallback operational case)
   - 800 = Operation complete (OPC)
   
   GPS time should only be used if the &lt;status> field is 403, 404 or 405  <br/> 
   Other GPS data could be used if the &lt;status> field is 401, 403 or 405
   
 - Errors:
   - -100 = Command syntax error
   - -101 = Command buffer overflow
   - -102 = SCPI feature not supported
   - -103 = SCPI parser table error (software programming error)
   - -200 = Command execution error
   - -400 = No GPS fix or time sync to either PPS or RTC
   