/******************************************************************************
 * Mainpage.h
 * Copyright (c) 2020 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Main Page for Doxygen documentation.  Not intended to be compiled or executed.
 */

#ifndef _MAINPAGE_H
#define _MAINPAGE_H

/**
 * @mainpage
 * Arduino-based GPS time sync application intended for amateur radio portable operation.
 *
 * Provides time and other GPS-derived data over a serial port. Used to  
 * synchronize computer time to UTC for portable operation when an internet 
 * connection is not available.
 *
 * Provides an SCPI-like API for command and control. (SCPI = Standard 
 * Commands for Programmable Instruments, a popular standard for test 
 * equipment programming.)  The host computer periodically sends a SCPI
 * command over the serial port to request time and updates its internal 
 * time accordingly.  Example command: :GPS:TIME?
 *
 * The GPS device is designed for use at a fixed location.  NMEA updates 
 * are slow (once every 5 seconds) to save processing time and (eventually)
 * allow for lower power consumption for portable operation.
 *
 * Hardware:
 *   - Adafruit Feather M0 board - based on Microchip ATSAMD21 Cortex M0   
 *   - Adafruit Ultimate GPS module - based on MediaTek MTK3339 chipset
 *   - Optional Adafruit FeatherWing 128x32 OLED display
 *
 * IDE:
 *   - Arduino IDE 1.8.12+
 *
 * Program Flow:
 * - As an Arduino application, the two primary functions are setup() and loop().
 *   - setup() provides program initialization
 *   - loop() provides runtime operation
 */
 

#endif // _MAINPAGE_H
