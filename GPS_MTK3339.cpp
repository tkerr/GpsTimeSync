/******************************************************************************
 * GPS_MTK3339.cpp
 * Copyright (c) 2022 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Implements an Arduino GPS interface class based on the MediaTek MTK3339 
 * chipset.
 *
 * See GPS_MTK3339.h for details.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include "Arduino.h"


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "GPS_MTK3339.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/
 
#define GPS_TIMEOUT_MS (5000)

// GPS module commands to select NMEA output sentences.
#define PMTK_SET_NMEA_OUTPUT_ALLDATA   "$PMTK314,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0*28"
#define PMTK_SET_NMEA_OUTPUT_RMC1      "$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29"
#define PMTK_SET_NMEA_OUTPUT_RMC5      "$PMTK314,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*2D"
#define PMTK_SET_NMEA_OUTPUT_RMC5_GGA5 "$PMTK314,0,5,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28"
#define PMTK_SET_NMEA_OUTPUT_DEFAULT   "$PMTK314,-1*04"

// GPS module commands to set the update rate.
#define PMTK_SET_NMEA_UPDATE_1HZ     "$PMTK220,1000*1F"  // Update every second
#define PMTK_SET_NMEA_UPDATE_5_SEC   "$PMTK220,5000*1B"  // Update every 5 seconds
#define PMTK_SET_NMEA_UPDATE_10_SEC  "$PMTK220,10000*2F" // Update every 10 seconds

// GPS module commands to set the fix calculation rate.
#define PMTK_API_SET_FIX_CTL_1HZ  "$PMTK300,1000,0,0,0,0*1C"  // Once per second
#define PMTK_API_SET_FIX_CTL_5HZ  "$PMTK300,200,0,0,0,0*2F"   // Five per second


/******************************************************************************
 * Local objects and data.
 ******************************************************************************/
static GpsIsrPtr pps_callback = 0;  //!< Pointer to PPS ISR callback function
static void pps_isr(void);          //!< The actual PPS ISR function


/******************************************************************************
 * Public methods and functions.
 ******************************************************************************/

/**************************************
 * GPS_MTK3339::GPS_MTK3339
 **************************************/
GPS_MTK3339::GPS_MTK3339() :
    m_reset_pin(-1),
    m_pps_pin(-1),
    m_fix_pin(-1),
    m_print_nmea(false),
    m_pScpiSerial(0)
{
    GpsBasic();
}

/**************************************
 * GPS_MTK3339::begin
 **************************************/
bool GPS_MTK3339::begin(int reset_pin, int pps_pin, int fix_pin)
{
    if (reset_pin >= 0) m_reset_pin = reset_pin;
    if (pps_pin >= 0) m_pps_pin = pps_pin;
    if (fix_pin >= 0) m_fix_pin = fix_pin;
    
    // Initialize GPS hardware.
    if (m_pps_pin >= 0)
    {
        pinMode(m_pps_pin, INPUT);
        attachInterrupt(digitalPinToInterrupt(m_pps_pin), pps_isr, RISING);
    }
    if (m_fix_pin >= 0)
    {
        pinMode(m_fix_pin, INPUT);
    }
    if (m_reset_pin >= 0)
    {
        pinMode(m_reset_pin, OUTPUT);
        digitalWrite(m_reset_pin, LOW);
        delay(100);
        digitalWrite(m_reset_pin, HIGH);
        delay(100);
    }

    // Call base class constructor.
    bool ok = GpsBasic::begin();
    
    // Wait for any response form the GPS module.
    if (ok)
    {
        uint32_t start = millis();
        while ((millis() - start) < GPS_TIMEOUT_MS)
        {
            GpsBasic::NmeaType nmea = service();
            if (nmea != NMEA_NONE)
            {
                ok = true;
                break;
            }
        }
    }
    
    if (ok)
    {
        // Configure the GPS module.
        ok = pmtk_cmd(PMTK_SET_NMEA_UPDATE_1HZ);
        if (ok)
        {
            ok = pmtk_cmd(PMTK_API_SET_FIX_CTL_1HZ);
        }
        if (ok)
        {
            ok = pmtk_cmd(PMTK_SET_NMEA_OUTPUT_RMC5_GGA5);
        }
    }
    return ok;
}

/**************************************
 * GPS_MTK3339::service()
 **************************************/ 
GpsBasic::NmeaType GPS_MTK3339::service()
{
    NmeaType nmea = GpsBasic::service();
    if (nmea != GpsBasic::NMEA_NONE)
    {
        if (m_print_nmea)
        {
            m_pScpiSerial->println(m_nmea_buf);
        }
    }
    return nmea;
}

/**************************************
 * GPS_MTK3339::attachPpsInterrupt()
 **************************************/ 
void GPS_MTK3339::attachPpsInterrupt(GpsIsrPtr isr)
{
    pps_callback = isr;
}

    
/**************************************
 * GPS_MTK3339::detachPpsInterrupt()
 **************************************/ 
void GPS_MTK3339::detachPpsInterrupt()
{
    pps_callback = 0;
}


/**************************************
 * GPS_MTK3339::incrementTime()
 **************************************/ 
void GPS_MTK3339::incrementTime()
{
    static int DAYS[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    
    if (++m_data.second > 59)
    {
        m_data.second = 0;
        if (++m_data.minute > 59)
        {
            m_data.minute = 0;
            if (++m_data.hour > 23)
            {
                m_data.hour = 0;
                if (m_data.month == 2)
                {
                    DAYS[2] = isLeapYear(m_data.year) ? 29 : 28;
                }
                if (++m_data.day > DAYS[m_data.month])
                {
                    m_data.day = 1;
                    if (++m_data.month > 12)
                    {
                        m_data.month = 1;
                        ++m_data.year;
                    }
                }
            }
        }
    }
}


/******************************************************************************
 * Protected methods and functions.
 ******************************************************************************/


/*****************************************************************************
 * Private methods and functions.
 ******************************************************************************/
 
/**************************************
 * GPS_MTK3339::pmtk_cmd
 **************************************/
bool GPS_MTK3339::pmtk_cmd(const char* cmd)
{
    static const char CRLF[3] = {'\x0D', '\x0A', '\x00'};
   
    bool ok = false;
    
    // Send the command.
    sendCmd(cmd);
    sendCmd(CRLF);
    
    // Wait for the response.
    uint32_t start = millis();
    while ((millis() - start) < GPS_TIMEOUT_MS)
    {
        GpsBasic::NmeaType nmea = service();
        if (nmea == NmeaType::NMEA_UNKNOWN)
        {
            if (strncmp(m_nmea_buf, "$PMTK001", 8) == 0)
            {
                char* rspStr = strtok(&m_nmea_buf[9], ",");  // Read and discard the command
                rspStr = strtok(NULL, "*");                  // Get status code
                if (*rspStr == '3')
                {
                    // Command successful.
                    ok = true;
                }
                break;
            }
        }
    }
    return ok;
}


/*****************************************************************************
 * Interrupt service routines.
 *****************************************************************************/
 
/**************************************
 * pps_isr()
 **************************************/ 
static void pps_isr(void)
{
    if (pps_callback != 0) 
    {
        pps_callback();
    }
}


/**************************************
 * isLeapYear()
 **************************************/  
bool isLeapYear(int year)
{
    if ((year % 400) == 0)      {return true;}
    else if ((year % 100) == 0) {return false;}
    else if ((year % 4) == 0)   {return true;}
    return false;
}

// End of file.
