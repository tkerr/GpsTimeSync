@echo off

REM Batch script to invoke GPS Time Sync python script
REM Right-click on this file and select "Run as administrator"

REM Designed to interface with the AB3GY GPS Time Sync hardware device
REM Script periodically reads GPS time over a serial port and sets computer time
REM Interval is specified on the command line (currently set to 300 seconds)
REM See GpsTimeSync.py for details

REM Python is run in interactive mode so that errors and verbose messages are visible
REM Modifications needed to run on target computer:
REM  - Change directory to the directory containing the python script
REM  - If changing drives, must specify the drive prior to the cd command (stupid DOS trick)
REM  - Set the COM port correctly

D:
cd D:\dev\Arduino\GpsTimeSync
python -i GpsTimeSync.py -vp COM27 300
