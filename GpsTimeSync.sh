#!/bin/bash
# Batch script to invoke GPS Time Sync python script
# Must be run with elevated privilige: 
# Open a terminal window and type: sudo GpsTimeSync.sh

# Designed to interface with the AB3GY GPS Time Sync hardware device
# Script periodically reads GPS time over a serial port and sets computer time
# Interval is specified on the command line (currently set to 300 seconds)
# See GpsTimeSync.py for details

# Python is run in interactive mode so that errors and verbose messages are visible
# Modifications needed to run on target computer:
#  - Set the serial port correctly

python -i GpsTimeSync.py -vp /dev/ttyACM0 300
