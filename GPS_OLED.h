/******************************************************************************
 * GPS_OLED.h
 * Copyright (c) 2021 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Adafruit FeatherWing 128x32 OLED display interface class definition for the 
 * Arduino GpsTimeSync application.
 */

#ifndef _GPS_OLED_H
#define _GPS_OLED_H

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include "Adafruit_SSD1306.h"


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "GpsData.h"


/******************************************************************************
 * Public definitions.
 ******************************************************************************/

// Button pin assignments for Feather 32u4, M0, M4, nrf52840 and 328p
#define GPS_OLED_BUTTON_A (9)
#define GPS_OLED_BUTTON_B (6)
#define GPS_OLED_BUTTON_C (5)


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

 
/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/

/**
 * @class GPS_OLED_
 * @brief LED interface class for the Arduino GpsSync application.
 *
 * Provides methods to display time, coordinates and other data on the
 * OLED display. 
 *
 * Only one GPS_OLED_ object should exist in the application since it
 * expects to have exclusive use of hardware resources for manipulating
 * the OLED display
 */
class GPS_OLED_
{
public:

    /**
     * @brief Default constructor.
     */
    GPS_OLED_();
    
    /**
     * @brief Initialize the GPS_OLED_ object.
     */
    void begin();
    
    /**
     * @brief Periodic service routine.
     *
     * Call at regular intervals to service the display buttons.
     *
     * @return The button number that was pressed, or zero if no button was pressed.
     */
    int service();
    
    /**
     * @brief Compute the grid square from lat/lon data.
     * @return The 6-character grid square as a string.
     */
    const char* getGrid(const GpsData& data);
    
    /**
     * @brief Show the "Acquiring Satellites" message.
     */
    void showAcquiring();
    
    /**
     * @brief Show the current GPS latitude and longitude.
     */
    void showLatLon(const GpsData& data);
    
    /**
     * @brief Show system status
     */
    void showStatus(const GpsData& data);
    
    /**
     * @brief Show the current time.
     */
    void showTime(const GpsData& data);

private:

    Adafruit_SSD1306* m_pDisplay;
    
};


// Define a single GPS_OLED object for use by the application.
extern GPS_OLED_ GPS_OLED;

#endif // _GPS_OLED_H
