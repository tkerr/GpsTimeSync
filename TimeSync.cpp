/******************************************************************************
 * TimeSync.cpp
 * Copyright (c) 2020 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Function and interrupt service routine (ISR) implementation used to coordinate
 * time synchronization on the Arduino GpsTimeSync project.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include "Arduino.h"


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "GpsTimeSync.h"
#include "TickZero.h"
#include "TimeSync.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

GpsData gps_data;  // Global GPS data structure used for tracking time

timeSyncMaster_t g_timeSyncMaster = SYNC_NONE;


/******************************************************************************
 * Local objects and data.
 ******************************************************************************/
static uint32_t sPpsTimestamp = 0;         //!< Timestamp of last PPS interrupt
static const uint32_t PPS_TIMEOUT = 1050;  //!< PPS interrupt timeout for no-fix detection


/******************************************************************************
 * Public methods and functions.
 ******************************************************************************/


/*****************************************************************************
 * Private methods and functions.
 ******************************************************************************/


/*****************************************************************************
 * Interrupt service routines.
 ******************************************************************************/


/**************************************
 * gpsPpsIsr
 **************************************/
void gpsPpsIsr()
{
    // When the GPS has a fix, the PPS pin pulses high at the beginning of 
    // each second for 50-100ms.
    
    // GPS time sync takes priority over RTC.
    
    TickZero.setCount(0);          // Do this first to head off a simultaneous RTC interrupt
    g_timeSyncMaster = SYNC_GPS;   // GPS is the time sync master
    gpsModule.incrementTime();     // Increment time for this interrupt
    sPpsTimestamp = millis();      // Record timestamp of this interrupt
}


/**************************************
 * tickZeroIsr
 **************************************/
void tickZeroIsr()
{
    if (g_timeSyncMaster == SYNC_GPS)
    {
        // GPS is the time sync master.
        uint32_t elapsed = millis() - sPpsTimestamp;
        if (elapsed >= PPS_TIMEOUT)
        {
            // No PPS interrupts.  Assume GPS fix has been lost.
            g_timeSyncMaster = SYNC_RTC;   // RTC is now the time sync master
            gpsModule.incrementTime();     // Increment time for this interrupt
            while (elapsed >= 1000)
            {
                gpsModule.incrementTime(); // Increment time for missed PPS interrupt
                if (elapsed >= 1000) elapsed -= 1000;
            }
        }
    }
    else if (g_timeSyncMaster == SYNC_RTC)
    {
        // RTC is the time sync master.
        gpsModule.incrementTime();     // Increment time for this interrupt
    }
}
 
// End of file.
