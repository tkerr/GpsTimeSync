/******************************************************************************
 * GPS_SCPI.h
 * Copyright (c) 2020 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * SCPI command interface definition for the Arduino GpsTimeSync application.
 *
 * SCPI = Standard Commands for Programmable Instruments
 *
 * SCPI is a standard command line and remote control syntax for controlling
 * test equipment. SCPI is codified in IEEE-488.2 and is currently managed
 * by the IVI Foundation (Interchangeable Virtual Instruments).
 *
 * References:
 * - Wikipedia: https://en.wikipedia.org/wiki/Standard_Commands_for_Programmable_Instruments
 * - SCPI Consortium: https://www.ivifoundation.org/scpi/default.aspx
 * - SCPI-1999 Specification: https://www.ivifoundation.org/docs/scpi-99.pdf
 */

#ifndef _GPS_SCPI_H
#define _GPS_SCPI_H

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stdbool.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "ISerial.h"


/******************************************************************************
 * Public definitions.
 ******************************************************************************/

//! This macro defines the maximum length of the SCPI command buffer.
#define GPS_SCPI_LINELENGTH_MAX (64)

/**
 * @defgroup SCPIData SCPI command parser structures and enumerations
 * @{
 */
 
/**
 * @brief GPS SCPI operation status.
 */
typedef enum
{
    GPS_SCPI_NONE    = 0,     //!< No command parsed or operation executed
    
    // Operational status
    
    GPS_SCPI_FIXONLY = 401,   //!< GPS fix but no time sync to PPS or RTC
    GPS_SCPI_PPSONLY = 402,   //!< No GPS fix but time sync to PPS (transitional case)
    GPS_SCPI_FIXPPS  = 403,   //!< GPS fix and time sync to PPS (primary operational case)
    GPS_SCPI_RTCONLY = 404,   //!< No GPS fix, time sync to RTC (fallback operational case)
    GPS_SCPI_FIXRTC  = 405,   //!< GPS fix, time sync to RTC (fallback operational case)
    
    GPS_SCPI_OPC     = 800,   //!< Operation complete (OPC)
    
    // Error status
    
    GPS_SCPI_CMD_ERR = -100,  //!< Command syntax error or unrecognized command
    GPS_SCPI_CMD_OVF = -101,  //!< Command buffer overflow
    GPS_SCPI_CMD_NSP = -102,  //!< SCPI feature not supported
    GPS_SCPI_PAR_ERR = -103,  //!< SCPI parser table error (software programming error)
    GPS_SCPI_EXC_ERR = -200,  //!< Command execution error
    GPS_SCPI_NOSYNC  = -400,  //!< No GPS fix or time sync to either PPS or RTC
} gps_scpi_status_t;


/**
 * @brief GPS SCPI command processing function pointer type definition.
 *
 * Points to a function used to perform final parsing and execution of a GPS 
 * SCPI command.
 *
 * The processing function must have the following signature:
 * @code{.c}
     gps_scpi_status_t functionName(char* prmStr, bool query);
 * @endcode
 * Arguments:
 *   - prmStr is a pointer to a string of parameters to be parsed
 *   - query is true if the command is a query (it ended with a '?')
 * Returns:
 *   - The function return status as a gps_scpi_status_t enumerated type
 */
typedef gps_scpi_status_t(*ScpiProcFn)(char* prmStr, bool query);


/**
 * @brief GPS SCPI parsing table entry definition.
 *
 * An entry consists of the following:
 *   - A short form command mnemonic in all caps (do not include the '?' for a query)
 *   - The next parsing table to use upon a mnemonic match, or zero if none
 *   - The final command processing function to use upon a mnemonic match, or zero if none
 *
 * Except for the table final entry, one (and only one) of pNextTable or procFn must be nonzero.
 *
 * Every table must end with a null entry in which the mnemonic is a null string.
 */
typedef struct gps_scpi_parser_entry_t
{
    const char*                     mnemonic;   //!< The short form command mnemonic in all caps
    const gps_scpi_parser_entry_t*  nextTable;  //!< The next level parsing table
    ScpiProcFn                      procFn;     //!< Function pointer to final command processing function
} gps_scpi_parser_entry_t;

/** @}*/


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

 
/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/

/**
 * @class GPS_SCPI
 * @brief SCPI command interface definition for the gps_time application.
 *
 * Provides a simple remote control interface to the gps_time hardware over 
 * a serial port. The command syntax resembles a simplified version of the 
 * SCPI standard.  Limitations are described below.
 *
 * Uses an Arduino Serial object defined by the GPS_SCPI_SERIAL macro.
 * The underlying serial object must be initialized by the user prior to
 * using this class.
 *
 * Since this class uses a specific hardware resource, there should only be
 * one instance of the class in the application.
 *
 * Limitations:
 *   -# Only short form command mnemonics in all caps are supported.
 *      Long form commands with lower case are not supported.
 *   -# Linking commands with a semicolon ';' is not supported. Commands
 *      are executed one at a time and are not chained together.
 *   -# Not all IEEE mandated commands are supported.
 *   -# Not all SCPI required commands are supported.
 */
class GPS_SCPI
{
public:

    /**
     * @brief Default constructor.
     */
    GPS_SCPI();
    
    /**
     * @brief Set the serial interface to be used by the SCPI interface.
     *
     * Note that the serial interface must be initialized separately.
     *
     * @param pSerial Pointer to the serial interface.
     */
    inline void setSerial(ISerial* pSerial) {m_pScpiSerial = pSerial;}
    
    inline ISerial* serial() {return m_pScpiSerial;}
    
    /**
     * @brief Initialize the GPS_SCPI object.
     *
     * The underlying serial port is not initialized and must be initialized
     * by the user prior to using this class.
     *
     * @return True if initialization successful, false otherwise.
     */
    bool begin();
    
    /**
     * @brief Clear the parser.
     */
    void clear();
    
    /**
     * @brief Enable/disable SCPI command echo.
     *
     * Intended for development and debug purposes only.
     *
     * @param enable True = command echo is enabled, false = disabled.
     */
    inline void cmdEcho(bool enable=true) {m_echo = enable;}
    
    /**
     * @brief Periodic service routine.
     *
     * Call at regular intervals to run the SCPI parser and execute commands.
     */
    void service();
    
    /**
     * @brief Print the specified status to the serial port.
     *
     * Intended to be used by command processing functions that encounter errors.
     *
     * @param status The status to print.
     */
    void printStatus(gps_scpi_status_t status);
    
private:
    
    /**
     * @brief Run the SCPI parser on the data in m_cmd and execute a command if parsed.
     */
    void parse();
    
    /**
     * @brief Recursive table-driven method to parse SCPI command mnemonics.
     *
     * See the definition for gps_scpi_parser_entry_t for additional details.
     *
     * @param table[] Table describing the current set of mnemonics to parse.
     */
    void parseMnemonic(const gps_scpi_parser_entry_t table[]);
    
    bool m_echo;                           //!< True if command echo is enabled
    char m_index;                          //!< Index into command buffer
    char m_cmd[GPS_SCPI_LINELENGTH_MAX];   //!< SCPI command buffer
    ISerial* m_pScpiSerial;                //!< Pointer to the SCPI serial interface
};


/**
 * @brief Find the start of a parameter in an input string.
 *
 * Searches the input string for the first non-whitespace character or end of
 * string and returns a pointer to it.
 *
 * Supply the result of this function to getParamEnd() to find the end of the
 * non-whitespace sequence in the string.
 *
 * @param str The string to search
 *
 * @return Pointer to the first non-whitespace character in str, or a null
 * string if not found.
 */
char* getParamStart(char* str);


/**
 * @brief Find the end of a parameter in an input string.
 *
 * Searches the input string for the first whitespace character or end of
 * string and returns a pointer to it.
 *
 * Supply the result of getParamStart() to this this function to find the end
 * of the non-whitespace sequence in the string.
 *
 * @param str The string to search
 *
 * @return Pointer to the first whitespace character in str, or a null
 * string if not found.
 */
char* getParamEnd(char* str);


#endif // _GPS_SCPI_H
