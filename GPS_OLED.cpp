/******************************************************************************
 * GPS_OLED.cpp
 * Copyright (c) 2021 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Adafruit FeatherWing 128x32 OLED display interface class implementation for  
 * the Arduino GpsTimeSync application.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include "Arduino.h"
#include "Wire.h"
#include "Adafruit_GFX.h"
#include "strFmt.h"



/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "GpsTimeSync.h"
#include "GPS_OLED.h"
#include "TimeSync.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/
GPS_OLED_ GPS_OLED;  //!< Global GPS_OLED object for the application to use


/******************************************************************************
 * Local objects and data.
 ******************************************************************************/
static const char* DIGITS[60] = 
{
    "00", "01", "02", "03", "04", "05", "06", "07", "08", "09",
    "10", "11", "12", "13", "14", "15", "16", "17", "18", "19",
    "20", "21", "22", "23", "24", "25", "26", "27", "28", "29",
    "30", "31", "32", "33", "34", "35", "36", "37", "38", "39",
    "40", "41", "42", "43", "44", "45", "46", "47", "48", "49",
    "50", "51", "52", "53", "54", "55", "56", "57", "58", "59",
};

/******************************************************************************
 * Public methods and functions.
 ******************************************************************************/


/**************************************
 * GPS_OLED_::GPS_OLED_()
 **************************************/  
GPS_OLED_::GPS_OLED_() :
    m_pDisplay(0)
{
    // Nothing to do.
}


/**************************************
 * GPS_OLED_::begin()
 **************************************/ 
void GPS_OLED_::begin()
{
    pinMode(GPS_OLED_BUTTON_A, INPUT_PULLUP);
    pinMode(GPS_OLED_BUTTON_B, INPUT_PULLUP);
    pinMode(GPS_OLED_BUTTON_C, INPUT_PULLUP);
    
    if (m_pDisplay == 0)
    {
        m_pDisplay = new Adafruit_SSD1306(128, 32, &Wire);  // 128x32 OLED matrix
        m_pDisplay->begin(SSD1306_SWITCHCAPVCC, 0x3C);      // I2C address = 0x3C
        m_pDisplay->setTextColor(SSD1306_WHITE);            // Display is monochrome
        m_pDisplay->clearDisplay();
    }
}


/**************************************
 * GPS_OLED_::service()
 **************************************/ 
int GPS_OLED_::service()
{
    int btn = 0;

    // Read OLED module buttons.
    if(!digitalRead(GPS_OLED_BUTTON_A))
    {
        btn = GPS_OLED_BUTTON_A;
    }
    else if(!digitalRead(GPS_OLED_BUTTON_B))
    {
        btn = GPS_OLED_BUTTON_B;
    }
    else if(!digitalRead(GPS_OLED_BUTTON_C))
    {
        btn = GPS_OLED_BUTTON_C;
    }

    return btn;
}


/**************************************
 * GPS_OLED_::getGrid()
 **************************************/ 
const char* GPS_OLED_::getGrid(const GpsData& data)
{
    static const char* UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    static const char* LOWER = "abcdefghijklmnopqrstuvwxyz";
    static char grid[7] = {0, 0, 0, 0, 0, 0, 0};
    
    float lat = data.lat_d + 90.0f;
    float lon = data.lon_d + 180.0f;

    // Compute grid char 1: uppercase longitude letter.
    grid[0] = '?';
    int lon1i = int(lon) / 20;
    if ((lon1i >= 0) && (lon1i < 26)) grid[0] = UPPER[lon1i];

    // Compute grid char 3: longitude number.
    grid[2] = '?';
    float lon_remainder = lon - (float(lon1i) * 20.0f);
    int lon3i = int(lon_remainder) / 2;
    if ((lon3i >= 0) && (lon3i < 10)) grid[2] = '0' + char(lon3i);

    // Compute grid char 5: lowercase longitude letter.
    grid[4] = '?';
    lon_remainder -= (float(lon3i) * 2.0f);
    int lon5i = int(lon_remainder * 12.0f);
    if ((lon5i >= 0) && (lon5i < 26)) grid[4] = LOWER[lon5i];

    // Compute grid char 2: uppercase latitude letter.
    grid[1] = '?';
    int lat2i = int(lat) / 10;
    if ((lat2i >= 0) && (lat2i < 26)) grid[1] = UPPER[lat2i];

    // Compute grid char 4: latitude number.
    grid[3] = '?';
    float lat_remainder = lat - (float(lat2i) * 10.0f);
    int lat4i = int(lat_remainder);
    if ((lat4i >= 0) && (lat4i < 10)) grid[3] = '0' + char(lat4i);

    // Compute grid char 6: lowercase latitude letter.
    grid[5] = '?';
    int lat6i = int(lat_remainder * 24.0);
    if ((lat6i >= 0) && (lat6i < 26)) grid[5] = LOWER[lat6i];

    return grid;
}


/**************************************
 * GPS_OLED_::showAcquiring()
 **************************************/ 
void GPS_OLED_::showAcquiring()
{
    m_pDisplay->clearDisplay();
    m_pDisplay->setTextSize(2);
    m_pDisplay->setCursor(0,0);
    m_pDisplay->print("Acquiring Satellites");
    m_pDisplay->display();
}


/**************************************
 * GPS_OLED_::showLatLon()
 **************************************/ 
void GPS_OLED_::showLatLon(const GpsData& data)
{
    char buf[24];
    int len = strFmtFloat(buf, data.lat_d, 6);
    buf[len++] = '\n';
    strFmtFloat(&buf[len], data.lon_d, 6);
    m_pDisplay->clearDisplay();
    m_pDisplay->setTextSize(2);
    m_pDisplay->setCursor(0,0);
    m_pDisplay->print(buf);
    m_pDisplay->display();
}


/**************************************
 * GPS_OLED_::showStatus()
 **************************************/ 
void GPS_OLED_::showStatus(const GpsData& data)
{
    char buf[24];
    strFmtUint(buf, data.year);
    strcat(buf, "-");
    strcat(buf, DIGITS[data.month]);
    strcat(buf, "-");
    strcat(buf, DIGITS[data.day]);
    strcat(buf, " ");
    strcat(buf, DIGITS[data.hour]);
    strcat(buf, ":");
    strcat(buf, DIGITS[data.minute]);
    strcat(buf, ":");
    strcat(buf, DIGITS[data.second]);
    
    m_pDisplay->clearDisplay();
    m_pDisplay->setTextSize(1);
    m_pDisplay->setCursor(0,0);
    
    m_pDisplay->print(buf);
    strFmtFloat(buf, data.alt, 1);
    m_pDisplay->print("\nAlt:  ");
    m_pDisplay->print(buf);
    
    m_pDisplay->print("\nSync: ");
    switch(g_timeSyncMaster)
    {
    case SYNC_GPS:
        m_pDisplay->print("GPS (");
        m_pDisplay->print(DIGITS[data.numsats]);
        m_pDisplay->print(")");
        break;
    case SYNC_RTC:
        m_pDisplay->print("RTC");
        break;
    default:
        m_pDisplay->print("NONE");
        break;
    }
    
    strFmtFloat(buf, data.hdop, 1);
    m_pDisplay->print("\nHdop: ");
    m_pDisplay->print(buf);
    m_pDisplay->print(" Grid ");
    m_pDisplay->print(getGrid(data));
    
    m_pDisplay->display();
}


/**************************************
 * GPS_OLED_::showTime()
 **************************************/ 
void GPS_OLED_::showTime(const GpsData& data)
{
    char now[6];
    strcpy(now, DIGITS[data.hour]);
    strcat(now, ":");
    strcat(now, DIGITS[data.minute]);
    m_pDisplay->clearDisplay();
    m_pDisplay->setTextSize(4);
    m_pDisplay->setCursor(0,0);
    m_pDisplay->print(now);
    m_pDisplay->display();
}


/*****************************************************************************
 * Private methods and functions.
 ******************************************************************************/


/*****************************************************************************
 * Interrupt service routines.
 *****************************************************************************/
 
 
// End of file.
