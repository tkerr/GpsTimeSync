/******************************************************************************
 * GPS_SCPI.cpp
 * Copyright (c) 2020 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * SCPI command interface implementation for the Arduino GpsTimeSync application.
 *
 * See GPS_SCPI.h for details.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <ctype.h>
#include <string.h>
#include "Arduino.h"


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "GPS_SCPI.h"
#include "GPS_SCPI_Tables.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/


/******************************************************************************
 * Local objects and data.
 ******************************************************************************/


/******************************************************************************
 * Public methods and functions.
 ******************************************************************************/
 
 
/**************************************
 * toUpper()
 **************************************/ 
char toUpper(char c)
{
    if ((c >= 'a') && (c <= 'z'))
    {
        c -= 32;
    }
    return c;
}


/**************************************
 * GPS_SCPI::GPS_SCPI()
 **************************************/  
GPS_SCPI::GPS_SCPI() :
    m_echo(false),
    m_pScpiSerial(0)
{
    clear();
}


/**************************************
 * GPS_SCPI::begin()
 **************************************/ 
bool GPS_SCPI::begin()
{
    bool init_ok = true;
    clear();
    return init_ok;
}


/**************************************
 * GPS_SCPI::clear()
 **************************************/ 
void GPS_SCPI::clear()
{
    m_index  = 0;
    *m_cmd   = 0;
}


/**************************************
 * GPS_SCPI::service()
 **************************************/ 
void GPS_SCPI::service()
{
    // Check for characters to read.
    while (m_pScpiSerial->available())
    {
        // Read character.
        char c = m_pScpiSerial->read();
        
        // Check for overflow.
        if (m_index >= GPS_SCPI_LINELENGTH_MAX-1)
        {
            clear();
            printStatus(GPS_SCPI_CMD_OVF);
            break;
        }
        
        // Check for end of command line.
        else if ((c == '\r') || (c == '\n'))
        {
            m_cmd[m_index] = 0;
            if (m_index > 0)
            {
                if (m_echo) m_pScpiSerial->println(m_cmd);
                parse();
            }
            
            // Reset command buffer.
            m_index = 0;
            *m_cmd  = 0;
        }
        
        // Add character to buffer.
        else
        {
            m_cmd[m_index++] = toUpper(c);
            m_cmd[m_index]   = 0;
        }
    }
}


/**************************************
 * GPS_SCPI::printStatus()
 **************************************/ 
void GPS_SCPI::printStatus(gps_scpi_status_t status)
{
    m_pScpiSerial->println(status);
}


/**************************************
 * getParamStart()
 **************************************/
char* getParamStart(char* str)
{
    char* p = str;
    char  c = *p;
    while (isspace(c) && (c != 0))
    {
        p++;
        c = *p;
    }
    return p;
}


/**************************************
 * getParamEnd()
 **************************************/
char* getParamEnd(char* str)
{
    char* p = str;
    char  c = *p;
    while (!isspace(c) && (c != 0))
    {
        p++;
        c = *p;
    }
    return p;
}


/*****************************************************************************
 * Private methods and functions.
 ******************************************************************************/


/**************************************
 * GPS_SCPI::parse()
 **************************************/ 
void GPS_SCPI::parse()
{
    m_index = 0;
    
    char c = *m_cmd;
    
    if (c == '*')
    {
        m_index = 1;
        parseMnemonic(CommonCommandTable);
    }
    else if (c == ':')
    {
        m_index = 1;
        parseMnemonic(RootTable);
    }
    else
    {
        parseMnemonic(RootTable);
    }
}


/**************************************
 * GPS_SCPI::parseMnemonic()
 **************************************/ 
void GPS_SCPI::parseMnemonic(const gps_scpi_parser_entry_t table[])
{
    int   index   = 0;      // Parser table index
    int   tbl_len = 0;      // Parser table mnemonic length
    bool  match   = false;  // True if mnemonic match found
    bool  query   = false;  // True if command is a query (ends with '?')
    char* cmd_mn  = &m_cmd[m_index];  // Pointer to current mnemonic location in command
    int   cmd_len = 0;                // Current mnemonic length
    
    gps_scpi_status_t status = GPS_SCPI_NONE; // Internal parse status
    
    // Get current command mnemonic.
    char c = m_cmd[m_index];
    bool parsing = true;
    while (parsing)
    {
        if (c == '\0')
        {
            // End of string.
            parsing = false;
        }
        else if (c == ':')
        {
            // End of mnemonic.
            m_cmd[m_index] = 0;
            m_index++;  // Advance to next mnemonic in string
            parsing = false;
        }
        else if (c == ' ')
        {
            // End of command, parameters follow.
            m_cmd[m_index] = 0;
            m_index++;
            parsing = false;
        }
        else if (c == '?')
        {
            // End of query.
            m_cmd[m_index] = 0;
            query = true;
            parsing = false;
        }
        else if (c == ';')
        {
            // Command linking is not supported.
            m_cmd[m_index] = 0;
            printStatus(GPS_SCPI_CMD_NSP);
            return; // NOTE: Exiting method here
        }
        else
        {
            // Add character to mnemonic.
            cmd_len++;
            m_index++;
            c = m_cmd[m_index];
        }
    }
    //Serial.print("1>"); Serial.print(cmd_mn); Serial.print(','); Serial.println(cmd_len);
    
    // Iterate through the parser table and look for a mnemonic match.
    do
    {
        // Get the table entry.
        const char* tbl_mn = table[index].mnemonic;
        const gps_scpi_parser_entry_t* nextTable = table[index].nextTable;
        ScpiProcFn fn = table[index].procFn;
        tbl_len = strlen(tbl_mn);
        index++;
        
        //Serial.print("2>"); Serial.print(tbl_mn); Serial.print(','); Serial.println(tbl_len);
                
        // End of table.
        if (tbl_len == 0) break;
        
        // Sanity check the table entry.
        if ((nextTable == 0) && (fn == 0))
        {
            // Both entries can't be zero.
            status = GPS_SCPI_PAR_ERR;
            printStatus(GPS_SCPI_PAR_ERR);
            break;
        }
        else if ((nextTable != 0) && (fn != 0))
        {
            // Both entries can't be nonzero.
            status = GPS_SCPI_PAR_ERR;
            printStatus(GPS_SCPI_PAR_ERR);
            break;
        }
        
        // Check for a mnemonic match in the table.
        if (tbl_len == cmd_len)
        {
            if (strncmp(cmd_mn, tbl_mn, cmd_len) == 0)
            {
                // Found a match.
                match = true;
                //Serial.println("MATCH");
                if (nextTable)
                {
                    // Parse the next level mnemonic.
                    parseMnemonic(nextTable);
                }
                else
                {
                    // Execute the final command processing function.
                    //Serial.print("QUERY: "); Serial.println(query);
                    status = fn(&m_cmd[m_index], query);
                }
            }
        }
    } while ((tbl_len > 0) && !match);
    
    if (!match)
    {
        if (status != GPS_SCPI_PAR_ERR)
        {
            // No mnemonic match found.
            printStatus(GPS_SCPI_CMD_ERR);
        }
    }
}


/*****************************************************************************
 * Interrupt service routines.
 *****************************************************************************/
 
 
// End of file.
