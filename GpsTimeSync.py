###############################################################################
# GpsTimeSync.py
# Copyright (c) 2020 Tom Kerr AB3GY
#
# Python time synchronization script.
# Synchronizes computer time to the AB3GY GpsTimeSync device over a serial port.
# See print_usage() for script usage syntax.
#
# Designed for personal use by the author, but available to anyone under the
# license terms below.
#
# Released under the MIT License (MIT). 
# See http://opensource.org/licenses/MIT
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
###############################################################################

# System level packages.
import ctypes
import datetime
import getopt
import os.path
import sys
import threading
import time


##############################################################################
# Runtime environment checks.
##############################################################################

# Get the pyserial module.
try:
    import serial
except ModuleNotFoundError:
    print("ERROR: 'serial' module not found.")
    print("Try installing using 'pip install pyserial'")
    sys.exit(1)

# Get Linux-specific packages.
if (sys.platform == "linux2"):
    import calendar
    import ctypes.util

    # /usr/include/linux/time.h:
    #
    # define CLOCK_REALTIME                     0
    CLOCK_REALTIME = 0

    # /usr/include/time.h
    #
    # struct timespec
    #  {
    #    __time_t tv_sec;            /* Seconds.  */
    #    long int tv_nsec;           /* Nanoseconds.  */
    #  };
    class timespec(ctypes.Structure):
        _fields_ = [("tv_sec",  ctypes.c_long),
                    ("tv_nsec", ctypes.c_long)]
    librt = ctypes.CDLL(ctypes.util.find_library("rt"))

# Get Windows-specific packages.
elif (sys.platform == "win32"):
    try:
        import win32api
    except ModuleNotFoundError:
        print("ERROR: 'win32api' module not found.")
        print("Try installing using 'pip install pywin32'")
        sys.exit(1)
        
    # Check for administrator privilege.
    if not ctypes.windll.shell32.IsUserAnAdmin():
        print("ERROR: This script requires administrator privilege to run.")
        sys.exit(1)
        
# Unsupported platform.
else:
    print("ERROR: Unsupported platform: " + sys.platform)
    sys.exit(1)


##############################################################################
# Global objects and data.
##############################################################################

# Get script name.
basename = os.path.basename(sys.argv[0])

# Set the default serial port name.
portname = "/dev/ttyACM0"
if (sys.platform == "win32"):
    portname = "COM1"

# The serial port object.
ser = serial.Serial()

# Script execution parameters. 
baudrate   = 115200 # Serial port baud rate
fatal      = False  # Fatal error, kill the thread
printCoord = False  # Print GPS coordinates
verbose    = False  # Verbose printing if true, quiet if false
offset = 0 # Time offset in seconds, used to adjust to DX stations with inaccurate clocks


##############################################################################
# Functions.
##############################################################################

# ----------------------------------------------------------------------------
def print_usage():
    global basename
    global baudrate
    global portname
    print("")
    print("Usage: " + basename + " [-bhopv] <seconds>")
    print("Synchronize computer time to the AB3GY GpsTimeSync device over a serial port.")
    print("<seconds> is the synchronization interval in seconds.")
    print("          Set to zero to synchronize once and exit.")
    print("Options:")
    print("  -b baud = Set the serial port baud rate (default = " + str(baudrate) + ")")
    print("  -c = Print GPS coordinates at each time interval")
    print("  -h = Print this message and exit")
    print("  -o secs = Add this offset to the time setting (can be negative)")
    print("  -p port = Set the serial port name (default = " + portname + ")")
    print("  -v = Enable verbose output (default = run silently)")
    sys.exit(1)


# ----------------------------------------------------------------------------
def _linux_set_time(gpsTime):
    global fatal
    global CLOCK_REALTIME
    global librt
    
    ok = True
    time_struct = datetime.datetime(
        gpsTime[0], gpsTime[1], gpsTime[2],
        gpsTime[3], gpsTime[4], gpsTime[5]).timetuple()

    ts = timespec()
    ts.tv_sec = int( calendar.timegm( time_struct ) )
    ts.tv_nsec = gpsTime[6] * 1000000 # Millisecond to nanosecond

    # http://linux.die.net/man/3/clock_settime
    librt.clock_settime(CLOCK_REALTIME, ctypes.byref(ts))
    return ok


# ----------------------------------------------------------------------------
def _win32_set_time(gpsTime):
    global fatal
    
    ok = True
    today = datetime.datetime(gpsTime[0], gpsTime[1], gpsTime[2])
    dayOfWeek = today.isocalendar()[2]
    
    # Setting system time requires administrator privilege.
    try:
        win32api.SetSystemTime(
            gpsTime[0], gpsTime[1], dayOfWeek,  gpsTime[2], 
            gpsTime[3], gpsTime[4], gpsTime[5], gpsTime[6])
    except Exception as err:
        errmsg = str(err)
        print("ERROR: " + errmsg)
        fatal = True
        ok = False
    return ok


# ----------------------------------------------------------------------------
def check_device_id():
    global ser
    ok = True

    # Identification query.
    resp = serial_txn("*IDN?")
    if verbose: print("Hardware ID: " + resp)
    if ("AB3GY,GPSTimeSync" not in resp):
        print("ERROR: incorrect serial port identification: " + resp);
        ok = False
    return ok


# ----------------------------------------------------------------------------
def poll_wait(interval):
    global fatal
    elapsed = 0
    period = 10
    while (elapsed < interval) and not fatal:
        time.sleep(period)
        elapsed += period


# ----------------------------------------------------------------------------
def serial_txn(cmd):
    global fatal
    global ser
    
    resp = ""
    cmd += "\n"
    
    try:
        # Flush the read buffer.
        while (ser.in_waiting):
            ser.read(ser.in_waiting)

        # Send the command.
        ser.write(cmd.encode('UTF-8'))
    
        # Get the response.
        byte = ser.read(1).decode('UTF-8')
        while ((byte != "\r") and (byte != "\n")):
            resp += byte
            byte = ser.read(1).decode('UTF-8')
            
    except Exception as err:
        print("serial_txn error: " + str(err))
    
    # Return the response.
    return str(resp)


# ----------------------------------------------------------------------------
def set_time():
    global offset
    global verbose
    
    timeSet = False

    # Get the GPS time.
    resp = serial_txn(":GPS:TIME?")
    
    # Split the response into a list of integers.
    # Expecting exactly 8 integer fields as follows:
    # [year, month, day, hour, minute, second, millisecond, status]
    ts = list(map(int, resp.split(",")))
    if (len(ts) != 8):
        if ((len(ts) == 1) and (ts[0] == -400)):
            print("GPS acquiring satellites")
        else:
            print("ERROR: GpsTimeSync invalid time response: " + resp)
        return timeSet
        
    # Check the time sync status.
    status = ts[7]
    if (status == 403) or (status == 404) or (status == 405):
    
        # Add time offset if specified.
        if (abs(offset) > 0.001):
            epoch_time = datetime.datetime(
                year=ts[0],
                month=ts[1],
                day=ts[2],
                hour=ts[3],
                minute=ts[4],
                second=ts[5],
                microsecond=(ts[6] * 1000)
            ).timestamp()
            epoch_time += offset
            new_time = datetime.datetime.fromtimestamp(epoch_time)
            ts[0], ts[1], ts[2] = new_time.year, new_time.month, new_time.day
            ts[3], ts[4], ts[5] = new_time.hour, new_time.minute, new_time.second
            ts[6] = int(new_time.microsecond / 1000)
    
        # Set the system time.
        if (sys.platform == "win32"):
            timeSet = _win32_set_time(ts[:7])
        else:
            timeSet = _linux_set_time(ts[:7])
    else:
        if verbose:
            print("GPS time sync not ready, status = " + str(status))

    # if verbose: print(resp)    
    return timeSet
    
    
# ----------------------------------------------------------------------------
def time_sync_thread(interval):
    global ser
    global fatal
    global printCoord
    global verbose

    if verbose: print(basename + ": Thread start")
    
    try:
        ser.open()
    except Exception as err:
        print("ERROR opening serial port " + portname + ":")
        print(str(err))
        fatal = True
        
    # Make sure we are communicating with the AB3GY GpsTimeSync device.
    if check_device_id():
    
        if (interval > 0):
            # Set system time at regular intervals.
            while not fatal:
            
                # Print current coordinates.
                if printCoord: print("Coordinates: " + serial_txn(":GPS:COOR?"))
                
                # Set the time.
                ok = set_time()
                if ok:
                    if verbose: print("System time: " + time.strftime("%Y-%m-%d %H:%M:%S"));
                    poll_wait(interval)
                else:
                    poll_wait(30)
        else:
            # Set system time once then exit.
            set_time()
            if verbose: print("System time: " + time.strftime("%Y-%m-%d %H:%M:%S"));
            
    # Thread exit.            
    ser.close()
    if verbose: print(basename + ": Thread exit")


##############################################################################
# Main program.
############################################################################## 
if __name__ == "__main__":

    # Get command line options.
    # See print_usage() for details.
    try:
        (opts, args) = getopt.getopt(sys.argv[1:], "b:cho:p:v")
    except (getopt.GetoptError) as err:
        print(str(err))
        print_usage()
        
    for (o, a) in opts:
        if (o == "-b"):
            baudrate = a
        elif (o == "-c"):
            printCoord = True
        elif (o == "-h"):
            print_usage()
        elif (o == "-o"):
            offset = float(a)
        elif (o == "-p"):
            portname = a
        elif (o == "-v"):
            verbose = True

    # Check argument count.
    if (len(args) < 1):
        print("ERROR: Too few arguments")
        print_usage()
    
    # Get the update interval in seconds.
    intervalSecs = int(args[0])
    
    # Configure the serial port.
    ser.port     = portname
    ser.baudrate = baudrate
    ser.bytesize = serial.EIGHTBITS
    ser.parity   = serial.PARITY_NONE
    ser.stopbits = serial.STOPBITS_ONE
    ser.timeout  = 2.0
    
    # Create and start a thread to perform the GPS time sync.
    syncThread = threading.Thread(target=time_sync_thread, args=(intervalSecs,))
    syncThread.start()
    
# End of file.
