/******************************************************************************
 * Version.h
 * Copyright (c) 2020 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Application version information compatible with the SCPI IDN? response format.
 */

#ifndef _VERSION_H
#define _VERSION_H

/******************************************************************************
 * Version information.
 * All information is defined as strings.
 ******************************************************************************/
#define IDN_MFR      "AB3GY"
#define IDN_MODEL    "GPSTimeSync"
#define IDN_SERIAL   "0"
#define IDN_FIRMWARE "0.2"

#endif // _VERSION_H
