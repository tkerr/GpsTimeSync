/*****************************************************************************
 * GPS_MTK3339.h
 * Copyright (c) 2022 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ****************************************************************************/

/**
 * @file
 * @brief
 * Defines an Arduino GPS interface class based on the MediaTek MTK3339 
 * chipset.
 *
 * This is the chipset used in the Adafruit Ultimate GPS module.
 *
 * References:
 *  - GlobalTop PMTK command packet, Rev. A11, 24-Sep-2012
 *  - GlobalTop FGPMMOPA6H GPS Standalone Module Data Sheet, Rev. V0A, 2011
 *  - MediaTek MT3339 All-in-One GPS Datasheet, Version 1.0, 13-Jan-2017
 * 
 * Note: GlobalTop was acquired by Sierra Wireless in 2017.
 */
#ifndef _GPS_MTK3339_H_
#define _GPS_MTK3339_H_

/******************************************************************************
 * System include files.
 ******************************************************************************/


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "GpsBasic.h"


/******************************************************************************
 * Public definitions.
 ******************************************************************************/

/**
 * @brief Interrupt service routine (ISR) function type definition.
 *
 * Interrupt service routines must be functions that take no arguments
 * (void) and return no value (void).
 *
 * Example: void myIsr(void);
 */
typedef void(*GpsIsrPtr)(void); 


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

 
/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/

/**
 * @class GPS_MTK3339
 * @brief GPS interface class based on the MediaTek MTK3339 chipset.
 */
class GPS_MTK3339 : public GpsBasic
{
public:

    /**
     * @brief Default constructor.
     */
    GPS_MTK3339();
    
    /**
     * @brief Set the serial interface to be used by the SCPI interface.
     *
     * Note that the serial interface must be initialized separately.
     *
     * @param pSerial Pointer to the serial interface.
     */
    inline void setScpiSerial(ISerial* pSerial) {m_pScpiSerial = pSerial;}
    
    /**
     * @brief Initialize the GPS module.
     *
     * @param reset_pin Optional Arduino pin number of the GPS reset input pin.
     * @param pps_pin Optional Arduino pin number of the GPS 1 PPS output pin.
     * @param fix_pin Optional Arduino pin number of the GPS fix output pin.
     *
     * @return True if initialization successful, false otherwise.
     */
    virtual bool begin(int reset_pin = -1, int pps_pin = -1, int fix_pin = -1);
    
    /**
     * @brief Periodic service routine.
     *
     * Call at regular intervals to service the GPS hardware and provide
     * updates.
     *
     * @return The NMEA sentence type that was just parsed, or NMEA_NONE if none was parsed.
     */
    virtual GpsBasic::NmeaType service();
    
    /**
     * @brief Attach an interrupt service routine (ISR) to the PPS signal.
     *
     * When the GPS has a fix, its PPS output pin pulses high at the 
     * beginning of each second for 50-100ms.
     *
     * Attaching an ISR function to this interrupt calls the function 
     * when the PPS signal goes high.
     *
     * @param isr The ISR function to be called. The function must take no
     * arguments and return no value (i.e. void isr(void)).
     */
    void attachPpsInterrupt(GpsIsrPtr isr);
    
    /**
     * @brief Detach the interrupt service routine (ISR) from the PPS signal.
     *
     * See attachPpsInterrupt() for details.
     */
    void detachPpsInterrupt();
    
    /**
     * @brief Increment the time values in the GPS data structure by one second.
     */
    void incrementTime();
    
    /**
     * @brief Debug method to enable printing of NMEA sentences over the
     * Arduino Serial port.
     *
     * @param enable Set to true to enable debug printing, false to disable printing.
     */
    inline void printNmea(bool enable=true) {m_print_nmea=enable;}
    
protected:

private:

    /**
     * @brief Send a custom $PMTK command to the device.
     *
     * @param cmd The custom $PMTK command string to send.  
     * The CR/LF terminator is appended to the command string.
     *
     * @return True if command execution was successful, false otherwise.
     */
    bool pmtk_cmd(const char* cmd);
    
    int m_reset_pin;         //!< Arduino pin number of the GPS reset input pin
    int m_pps_pin;           //!< Arduino pin number of the GPS 1 PPS output pin
    int m_fix_pin;           //!< Arduino pin number of the GPS fix output pin
    bool m_print_nmea;       //!< Debug flag to print NMEA sentences
    ISerial* m_pScpiSerial;  //!< Pointer to serial interface used by the SCPI interface

};


/**
 * @brief Return true if year is a leap year, false otherwise.
 */
bool isLeapYear(int year);


#endif // _GPS_MTK3339_H_
