/******************************************************************************
 * GPS_LED.h
 * Copyright (c) 2020 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * LED interface class definition for the Arduino GpsTimeSync application.
 */

#ifndef _GPS_LED_H
#define _GPS_LED_H

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stdbool.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/


/******************************************************************************
 * Public definitions.
 ******************************************************************************/
 
/**
 * @brief LED event definitions used for turning LEDs on and off.
 */
typedef enum
{
    LED_ALLOFF = 0, //!< All LEDs off
    LED_ALLON,      //!< All LEDs on
    LED_NOFIX,      //!< No GPS fix
    LED_FIX,        //!< GPS fix attained
    LED_NOSYNC,     //!< No time synchronization
    LED_GPS_SYNC,   //!< Time is synchronized to GPS PPS signal
    LED_RTC_SYNC    //!< Time is synchronized to RTC
} led_event_t;


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

 
/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/

/**
 * @class GPS_LED_
 * @brief LED interface class for the Arduino GpsSync application.
 *
 * Provides methods to manipulate LEDs for GPS fix, GPS PPS and RTC status
 * for the GpsSync application. 
 *
 * Only one GPS_LED_ object should exist in the application since it
 * expects to have exclusive use of hardware GPIO resources for manipulating
 * the LEDs.
 */
class GPS_LED_
{
public:

    /**
     * @brief Default constructor.
     */
    GPS_LED_();
    
    /**
     * @brief Initialize the GPS_LED_ object.
     * 
     * Initializes the LED hardware and turns the LEDs off.
     */
    void begin();
    
    /**
     * @brief Enable/disable LEDs.
     *
     * Allow LEDs to be enabled or disabled depending on user preference.
     *
     * @param ena True = LEDs are enabled, false = LEDs are disabled.
     */
    void enable(bool ena=true);
    
    /**
     * @brief Set the LED event.
     *
     * Individual LEDs are turned on and off to reflect the specified event.
     *
     * @param event The event to set.
     */
    void setEvent(led_event_t event);
    
private:

    bool m_enabled; //!< LEDs enabled when true, disabled when false
    
};


// Define a single GPS_LED object for use by the application.
extern GPS_LED_ GPS_LED;

#endif // _GPS_LED_H
