/******************************************************************************
 * GpsTimeSync.ino
 * Copyright (c) 2020 Tom Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Arduino-based GPS time sync application intended for amateur radio portable operation.
 *
 * See Mainpage.h for details.
 */
 
/******************************************************************************
 * Lint options.
 ******************************************************************************/
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stdint.h>
#include "Arduino.h"

/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "GpsTimeSync.h"

#include "GPS_MTK3339.h"
#include "GPS_SCPI.h"
#include "TickZero.h"
#include "TimeSync.h"

// Conditional include files.
#ifdef HAS_GPS_LED
#include "GPS_LED.h"
#endif

#ifdef HAS_OLED_DISPLAY
#include "GPS_OLED.h"
#endif


/******************************************************************************
 * Forward references.
 ******************************************************************************/

/**
 * @defgroup ArduinoFlow Ardunio program flow functions
 * @{
 */ 
void setup();                          //!< Arduino application initialization function.
void loop();                           //!< Arduino application main loop function.
void printData(const GpsData& data);   //!< Print formatted GPS data.  Used for development and debug.
/** @}*/

static void gps_led_service();
static void gps_oled_service(bool update);


/******************************************************************************
 * Local definitions.
 ******************************************************************************/


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/
 
GPS_MTK3339 gpsModule;   //!< The global MTK3339 GPS module object

GPS_SCPI gpsScpiParser;  //!< The global SCPI parser object

// Global ISerial object for the GPS interface.
// This is the serial port that is connected to the GPS.
ISerial gps_serial;

// Global serial monitor object.
// The is the serial port that prints messages to the user.
ISerial monitor;


/******************************************************************************
 * Local objects and data.
 ******************************************************************************/


/******************************************************************************
 * Public functions.
 ******************************************************************************/

/**************************************
 * setup()
 **************************************/ 
void setup() 
{
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
    
#ifdef HAS_GPS_LED
    // GPS LED hardware initialization.
    GPS_LED.begin();
    GPS_LED.setEvent(LED_NOFIX);
    GPS_LED.setEvent(LED_NOSYNC);
#endif

#ifdef HAS_OLED_DISPLAY
    // GPS OLED display initialization.
    GPS_OLED.begin();
    GPS_OLED.showAcquiring();
#endif

    // Arduino serial monitor init.
    monitor.setSerial(&Serial);
    monitor.begin(MONITOR_SERIAL_BAUD);
    delay(2000);
    monitor.println("GPS Time Sync");
    
    // GPS serial interface init.
    // Use the serial port connected to the GPS module.
    gps_serial.setSerial(&Serial1);
    if (!gps_serial.begin(GPS_SERIAL_BAUD))
    {
        monitor.println("GPS serial port init FAILED.");
    }
    
    // Command parser initialization.
    gpsScpiParser.setSerial(&monitor);
    gpsScpiParser.begin();
    
    // GPS module initialization.
    gpsModule.setGpsSerial(&gps_serial);
    gpsModule.setScpiSerial(&monitor);
    if (!gpsModule.begin(GPS_RESET_PIN, GPS_1PPS_PIN, GPS_FIX_PIN))
    {
        Serial.println("GPS INIT ERROR");
        digitalWrite(LED_BUILTIN, HIGH);
    }
    gpsModule.attachPpsInterrupt(gpsPpsIsr);

    // RTC initialization.
    TickZero.begin();
    TickZero.attachInterrupt(tickZeroIsr);
}


/**************************************
 * loop()
 **************************************/ 
void loop() 
{
    // Perform periodic GPS module service.
    // Look for a $GPRMC sentence.
    bool update = (gpsModule.service() == GpsBasic::NMEA_GPRMC);

    // GPS LED service.
    gps_led_service();
    
    // GPS OLED display service.
    gps_oled_service(update);
    
    // Perform periodic command parser service.
    gpsScpiParser.service();
}


/**************************************
 * printData()
 **************************************/ 
void printData(const GpsData& data)
{
    char time[24];
    sprintf(time, "%04d-%02d-%02d %02d:%02d:%02d", 
        data.year, data.month, data.day,
        data.hour, data.minute, data.second);
    Serial.print("\ntime:  "); Serial.println(time);

    Serial.print("lat:   "); Serial.println(data.lat_d, 6);
    Serial.print("lon:   "); Serial.println(data.lon_d, 6);
    Serial.print("alt:   "); Serial.println(data.alt, 6);
    Serial.print("sats:  "); Serial.println(data.numsats);
    Serial.print("qual:  "); Serial.println(data.qual);
    Serial.print("speed: "); Serial.println(data.speed_k, 2);
    Serial.print("track: "); Serial.println(data.track, 2);
    Serial.print("var:   "); Serial.println(data.var, 2);
    Serial.print("hdop:  "); Serial.println(data.hdop, 2);
}


/******************************************************************************
 * Private functions.
 ******************************************************************************/

/**************************************
 * gps_led_service()
 **************************************/ 
static void gps_led_service()
{
#ifdef HAS_GPS_LED
    bool fix = GPS_MTK3339.hasFix();
    if (fix)
    {
        GPS_LED.setEvent(LED_FIX);
    }
    else
    {
        GPS_LED.setEvent(LED_NOFIX);
    }
    
    // Check the time sync master.
    switch (g_timeSyncMaster)
    {
    case SYNC_NONE:
    default:
        GPS_LED.setEvent(LED_NOSYNC);
        break;
    case SYNC_GPS:
        GPS_LED.setEvent(LED_GPS_SYNC);
        break;
    case SYNC_RTC:
        GPS_LED.setEvent(LED_RTC_SYNC);
        break;
    }
#endif 
}


/**************************************
 * gps_oled_service()
 **************************************/ 
static void gps_oled_service(bool update)
{
#ifdef HAS_OLED_DISPLAY
    static bool initial_fix = false;
    static int last_btn = 0;
    static int last_second = -1;
    
    // Do nothing until initial fix received.
    if (!initial_fix)
    {
        initial_fix = gpsModule.hasFix();
        if (initial_fix)
        {
            last_btn = GPS_OLED_BUTTON_A;
            update = true;  // Force a time update
        }
        else return;
    }

    // See if the seconds have changed.
    int second = gpsModule.data().second;
    if (last_second != second)
    {
        last_second = second;
        update = true;  // Force a display update
    }
    
    // See if active button has changed.
    int this_btn = GPS_OLED.service();
    if ((this_btn != 0) && (this_btn != last_btn))
    {
        last_btn = this_btn;
        update = true;  // Force a display update
    }

    // Update the display.
    if (update)
    {
        switch (last_btn)
        {
        case GPS_OLED_BUTTON_A:
            GPS_OLED.showTime(gpsModule.data());
            break;
        case GPS_OLED_BUTTON_B:
            GPS_OLED.showLatLon(gpsModule.data());
            break;
        case GPS_OLED_BUTTON_C:
            GPS_OLED.showStatus(gpsModule.data());
            break;
        default:
            break;
        }
    }

#endif 
}

// End of file.
