/******************************************************************************
 * GPS_LED.cpp
 * Copyright (c) 2020 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * LED interface class implementation for the Arduino GpsTimeSync application.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include "Arduino.h"


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "GPS_LED.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/
 
// LED pin definitions.
#define LED_INTERNAL_RED_PIN 13  //!< The LED on the Arduino Feather M0
#define LED_GPS_FIX_PIN      14  //!< GPS fix LED, green
#define LED_NOSYNC_PIN       15  //!< No master sync LED, red
#define LED_GPS_SYNC_PIN     16  //!< GPS master sync LED, green
#define LED_RTC_SYNC_PIN     17  //!< RTC master sync LED, yellow


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/
GPS_LED_ GPS_LED;  //!< Global GPS_LED object for the application to use


/******************************************************************************
 * Local objects and data.
 ******************************************************************************/


/******************************************************************************
 * Public methods and functions.
 ******************************************************************************/


/**************************************
 * GPS_LED_::GPS_LED_()
 **************************************/  
GPS_LED_::GPS_LED_() :
    m_enabled(true)
{
    // Nothing to do.
}


/**************************************
 * GPS_LED_::begin()
 **************************************/ 
void GPS_LED_::begin()
{
    // The Arduino on-board LED is active high.
    // All other LEDs are active low.
    // Initialize all LEDs to their off state.
    
    pinMode(LED_INTERNAL_RED_PIN, OUTPUT);
    digitalWrite(LED_INTERNAL_RED_PIN, LOW);
    
    pinMode(LED_GPS_FIX_PIN, OUTPUT);
    digitalWrite(LED_GPS_FIX_PIN, HIGH);
    
    pinMode(LED_NOSYNC_PIN, OUTPUT);
    digitalWrite(LED_NOSYNC_PIN, HIGH);
    
    pinMode(LED_GPS_SYNC_PIN, OUTPUT);
    digitalWrite(LED_GPS_SYNC_PIN, HIGH);
    
    pinMode(LED_RTC_SYNC_PIN, OUTPUT);
    digitalWrite(LED_RTC_SYNC_PIN, HIGH);
}


/**************************************
 * GPS_LED_::enable()
 **************************************/ 
void GPS_LED_::enable(bool ena)
{
    m_enabled = ena;
    if (!ena)
    {
        // Turn all LEDs off if disabled.
        digitalWrite(LED_GPS_FIX_PIN,  HIGH);
        digitalWrite(LED_NOSYNC_PIN,   HIGH);
        digitalWrite(LED_GPS_SYNC_PIN, HIGH);
        digitalWrite(LED_RTC_SYNC_PIN, HIGH);
    }
}


/**************************************
 * GPS_LED_::setEvent()
 **************************************/ 
void GPS_LED_::setEvent(led_event_t event)
{
    if (m_enabled)
    {
        switch (event)
        {
        case LED_ALLOFF:
        default:
            digitalWrite(LED_GPS_FIX_PIN,  HIGH);
            digitalWrite(LED_NOSYNC_PIN,   HIGH);
            digitalWrite(LED_GPS_SYNC_PIN, HIGH);
            digitalWrite(LED_RTC_SYNC_PIN, HIGH);
            break;
            
        case LED_ALLON:
            digitalWrite(LED_GPS_FIX_PIN,  LOW);
            digitalWrite(LED_NOSYNC_PIN,   LOW);
            digitalWrite(LED_GPS_SYNC_PIN, LOW);
            digitalWrite(LED_RTC_SYNC_PIN, LOW);
            break;
            
        case LED_NOFIX:
            digitalWrite(LED_GPS_FIX_PIN, HIGH);
            break;
            
        case LED_FIX:
            digitalWrite(LED_GPS_FIX_PIN, LOW);
            break;
            
        case LED_NOSYNC:
            digitalWrite(LED_NOSYNC_PIN,   LOW);
            digitalWrite(LED_GPS_SYNC_PIN, HIGH);
            digitalWrite(LED_RTC_SYNC_PIN, HIGH);
            break;
            
        case LED_GPS_SYNC:
            digitalWrite(LED_NOSYNC_PIN,   HIGH);
            digitalWrite(LED_GPS_SYNC_PIN, LOW);
            digitalWrite(LED_RTC_SYNC_PIN, HIGH);
            break;
            
        case LED_RTC_SYNC:
            digitalWrite(LED_NOSYNC_PIN,   HIGH);
            digitalWrite(LED_GPS_SYNC_PIN, HIGH);
            digitalWrite(LED_RTC_SYNC_PIN, LOW);
            break;
        }
    }
}


/*****************************************************************************
 * Private methods and functions.
 ******************************************************************************/


/*****************************************************************************
 * Interrupt service routines.
 *****************************************************************************/
 
 
// End of file.
