/******************************************************************************
 * TimeSync.h
 * Copyright (c) 2020 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Function and interrupt service routine (ISR) definitions used to coordinate
 * time synchronization on the Arduino GpsTimeSync project.
 */

#ifndef _TIME_SYNC_H
#define _TIME_SYNC_H

/******************************************************************************
 * System include files.
 ******************************************************************************/


/******************************************************************************
 * Local include files.
 ******************************************************************************/


/******************************************************************************
 * Public definitions.
 ******************************************************************************/
 
/**
 * @brief Time sync master enumerated type.
 */
typedef enum
{
    SYNC_NONE = 0,    //!< No time sync master determined
    SYNC_GPS,         //!< GPS is the time sync master
    SYNC_RTC          //!< RTC is the time sync master
} timeSyncMaster_t;


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/
 
/**
 * @brief The time sync master.
 * Determines which interrupt increments the time (GPS or RTC).
 */
extern timeSyncMaster_t g_timeSyncMaster;

 
/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/

/**
 * @brief GPS_MTK3339 1PPS interrupt service routine.
 * 
 * Attach this function to the GPS 1PPS interrupt during application initialization.
 */
void gpsPpsIsr();

/**
 * @brief TickZero 1-second interrupt service routine.
 * 
 * Attach this function to the TickZero interrupt during application initialization.
 */
void tickZeroIsr();


#endif // _TIME_SYNC_H
