/******************************************************************************
 * GPS_SCPI_Tables.h
 * Copyright (c) 2020 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * SCPI command interface parsing tables and function definitions for the 
 * Arduino GpsTimeSync application.
 *
 * See GPS_SCPI.h for details.
 */

#ifndef _GPS_SCPI_TABLES_H
#define _GPS_SCPI_TABLES_H

/******************************************************************************
 * System include files.
 ******************************************************************************/


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "GPS_SCPI.h"


/******************************************************************************
 * Public definitions.
 ******************************************************************************/


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

/**
 * @defgroup ParsingTables Parsing tables
 * @{
 */
extern const gps_scpi_parser_entry_t CommonCommandTable[];  //!< IEEE-488.2 common command parsing table
extern const gps_scpi_parser_entry_t RootTable[];           //!< Top-level command mnemonic parsing table
extern const gps_scpi_parser_entry_t GpsTable[];            //!< :GPS:xxx mnemonic parsing table
extern const gps_scpi_parser_entry_t StatTable[];           //!< :STAT:xxx mnemonic parsing table
extern const gps_scpi_parser_entry_t SystTable[];           //!< :SYST:xxx mnemonic parsing table
/** @}*/


/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/


#endif // _GPS_SCPI_TABLES_H
