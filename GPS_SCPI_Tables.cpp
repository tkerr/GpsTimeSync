/******************************************************************************
 * GPS_SCPI_Tables.cpp
 * Copyright (c) 2020 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * SCPI command interface parsing tables and function implementation for the 
 * Arduino GpsTimeSync application.
 *
 * See GPS_SCPI.h and GPSTimeSCIPTables.h for details.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <string.h>
#include "Arduino.h"


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "GpsTimeSync.h"
#include "GPS_LED.h"
#include "GPS_MTK3339.h"
#include "GPS_SCPI_Tables.h"
#include "TickZero.h"
#include "TimeSync.h"
#include "Version.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/
 
/**
 * @defgroup CommandFunctions SCPI command implementation functions
 * @{
 */
static gps_scpi_status_t dummyFn(char* prmStr, bool query);

// IEEE-488.2 common commands.
static gps_scpi_status_t clsFn(char* prmStr, bool query);
static gps_scpi_status_t idnFn(char* prmStr, bool query);
static gps_scpi_status_t opcFn(char* prmStr, bool query);
static gps_scpi_status_t rstFn(char* prmStr, bool query);

// Root level commands (:xxx)
static gps_scpi_status_t listFn(char* prmStr, bool query);

// :GPS:xxx commands.
static gps_scpi_status_t gpsCmdFn(char* prmStr, bool query);
static gps_scpi_status_t gpsCoorFn(char* prmStr, bool query);
static gps_scpi_status_t gpsHeadFn(char* prmStr, bool query);
static gps_scpi_status_t gpsQualFn(char* prmStr, bool query);
static gps_scpi_status_t gpsTimeFn(char* prmStr, bool query);

// :STAT:xxx commands.
static gps_scpi_status_t statCondFn(char* prmStr, bool query);

// :SYST:xxx commands.
static gps_scpi_status_t systEchoFn(char* prmStr, bool query);
static gps_scpi_status_t systLedsFn(char* prmStr, bool query);
static gps_scpi_status_t systNmeaFn(char* prmStr, bool query);

// Helper functions.
static gps_scpi_status_t getStatCond();
static void printMnemonic(const gps_scpi_parser_entry_t* table, int level, char sep);
/** @}*/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/
extern GPS_SCPI gpsScpiParser;  // The global SCPI parser object


/******************************************************************************
 * SCPI command parsing tables.
 ******************************************************************************/
 
/*********************************************************
 * CommonCommandTable[]
 * IEEE-488.2 common commands beginning with '*'
 *********************************************************/
const gps_scpi_parser_entry_t CommonCommandTable[] = 
{
    {"CLS", 0, clsFn},
    {"IDN", 0, idnFn},
    {"OPC", 0, opcFn},
    {"RST", 0, rstFn},
    {"", 0, 0}  // End of table
};


/*********************************************************
 * RootTable[]
 * Top-level commands starting with ':' or no delimiter.
 *********************************************************/ 
const gps_scpi_parser_entry_t RootTable[] = 
{
    {"GPS",  GpsTable,  0},
    {"LIST", 0,    listFn},
    {"STAT", StatTable, 0},
    {"SYST", SystTable, 0},
    {"", 0, 0}  // End of table
};


/*********************************************************
 * GpsTable[]
 * :GPS:xxx mnemonics.
 *********************************************************/ 
const gps_scpi_parser_entry_t GpsTable[] = 
{
    {"CMD",  0, gpsCmdFn},
    {"COOR", 0, gpsCoorFn},
    {"HEAD", 0, gpsHeadFn},
    {"QUAL", 0, gpsQualFn},
    {"TIME", 0, gpsTimeFn},
    {"", 0, 0}  // End of table
};


/*********************************************************
 * StatTable[]
 * :STAT:xxx mnemonics.
 *********************************************************/ 
const gps_scpi_parser_entry_t StatTable[] = 
{
    {"COND", 0, statCondFn},
    {"", 0, 0}  // End of table
};


/*********************************************************
 * SystTable[]
 * :SYST:xxx mnemonics.
 *********************************************************/ 
const gps_scpi_parser_entry_t SystTable[] = 
{
    {"ECHO", 0, systEchoFn},
    {"LEDS", 0, systLedsFn},
    {"NMEA", 0, systNmeaFn},
    {"", 0, 0}  // End of table
};


/******************************************************************************
 * Local objects and data.
 ******************************************************************************/


/******************************************************************************
 * Public methods and functions.
 ******************************************************************************/


/*****************************************************************************
 * Private methods and functions.
 ******************************************************************************/
 
/**
 * @brief Dummy command function used for development.
 * @param prmStr Command parameters (if any) passed as a string.
 * @param query Command is a query if true.
 * @return Command execution status.
 */
static gps_scpi_status_t dummyFn(char* prmStr, bool query)
{
    gps_scpi_status_t status = GPS_SCPI_OPC;
    gpsScpiParser.printStatus(status);
    return status;
}

/**
 * @brief *CLS command implementation.
 * @param prmStr Command parameters (if any) passed as a string.
 * @param query Command is a query if true.
 * @return Command execution status.
 */
static gps_scpi_status_t clsFn(char* prmStr, bool query)
{
    gps_scpi_status_t status = GPS_SCPI_OPC;
    gpsScpiParser.clear();
    gpsScpiParser.printStatus(status);
    return status;
}

/**
 * @brief *IDN? command implementation.
 * @param prmStr Command parameters (if any) passed as a string.
 * @param query Command is a query if true.
 * @return Command execution status.
 */
static gps_scpi_status_t idnFn(char* prmStr, bool query)
{
    gps_scpi_status_t status = GPS_SCPI_CMD_ERR;
    if (query)
    {
        gpsScpiParser.serial()->print(IDN_MFR);
        gpsScpiParser.serial()->print(',');
        gpsScpiParser.serial()->print(IDN_MODEL);
        gpsScpiParser.serial()->print(',');
        gpsScpiParser.serial()->print(IDN_SERIAL);
        gpsScpiParser.serial()->print(',');
        gpsScpiParser.serial()->println(IDN_FIRMWARE);
        status = GPS_SCPI_OPC;
    }
    else
    {
        gpsScpiParser.printStatus(status);
    }
    return status;
}

/**
 * @brief *OPC command implementation.
 * @param prmStr Command parameters (if any) passed as a string.
 * @param query Command is a query if true.
 * @return Command execution status.
 */
static gps_scpi_status_t opcFn(char* prmStr, bool query)
{
    // Commands are executed one at a time without interruption,
    // and they are not linked.  OPC response is always immediate.
    // Doesn't really do anything.
    gps_scpi_status_t status = GPS_SCPI_OPC;
    gpsScpiParser.printStatus(status);
    return status;
}

/**
 * @brief *RST command implementation.
 * @param prmStr Command parameters (if any) passed as a string.
 * @param query Command is a query if true.
 * @return Command execution status.
 */
static gps_scpi_status_t rstFn(char* prmStr, bool query)
{
    // Resets the GPS.  
    // Does not reset the RTC - this requires a power cycle.
    gps_scpi_status_t status = GPS_SCPI_OPC;
    if (!gpsModule.begin())
    {
        status = GPS_SCPI_EXC_ERR;
    }
    gpsScpiParser.clear();
    gpsScpiParser.printStatus(status);
    return status;
}

/**
 * @brief :LIST command implementation. Prints the hierarchy of SCPI commands.
 * @param prmStr Command parameters (if any) passed as a string.
 * @param query Command is a query if true.
 * @return Command execution status.
 */
static gps_scpi_status_t listFn(char* prmStr, bool query)
{
    gps_scpi_status_t status = GPS_SCPI_OPC;
    
    // Print IEEE-488.2 common commands.
    printMnemonic(CommonCommandTable, 0, '*');
    
    // Print the root command hierarchy.
    printMnemonic(RootTable, 0, ':');
    return status;
}

/**
 * @brief :GPS:CMD command implementation.
 * @param prmStr The GPS command passed as a string.
 * @param query Command is a query if true (should always be false in this case).
 * @return Command execution status.
 */
static gps_scpi_status_t gpsCmdFn(char* prmStr, bool query)
{
    static const char CRLF[3] = {'\x0D', '\x0A', '\x00'};
    
    gps_scpi_status_t status = GPS_SCPI_EXC_ERR;
    if (!query)
    {
        if (gpsModule.sendCmd(prmStr) > 0)
        {
            // Make sure the NMEA CR/LF terminator is included in the command.
            if (gpsModule.sendCmd(CRLF) > 0)
            {
                status = GPS_SCPI_OPC;
            }
        }
    }
    else
    {
        status = GPS_SCPI_CMD_ERR;
    }
    gpsScpiParser.printStatus(status);
    return status;
}

/**
 * @brief :GPS:COOR? command implementation.
 * @param prmStr Command parameters (if any) passed as a string.
 * @param query Command is a query if true.
 * @return Command execution status.
 */
static gps_scpi_status_t gpsCoorFn(char* prmStr, bool query)
{
    gps_scpi_status_t status = GPS_SCPI_CMD_ERR;
    if (query)
    {
        status = getStatCond();
        if (status < 0)
        {
            gpsScpiParser.printStatus(status);
        }
        else
        {
            GpsData data = gpsModule.data();
            gpsScpiParser.serial()->print(data.lat_d, 6);
            gpsScpiParser.serial()->print(',');
            gpsScpiParser.serial()->print(data.lon_d, 6);
            gpsScpiParser.serial()->print(',');
            gpsScpiParser.serial()->print(data.alt, 6);
            gpsScpiParser.serial()->print(',');
            gpsScpiParser.serial()->println(status);
        }
    }
    else
    {
        gpsScpiParser.printStatus(status);
    }
    return status;
}

/**
 * @brief :GPS:HEAD? command implementation.
 * @param prmStr Command parameters (if any) passed as a string.
 * @param query Command is a query if true.
 * @return Command execution status.
 */
static gps_scpi_status_t gpsHeadFn(char* prmStr, bool query)
{
    gps_scpi_status_t status = GPS_SCPI_CMD_ERR;
    if (query)
    {
        status = getStatCond();
        if (status < 0)
        {
            gpsScpiParser.printStatus(status);
        }
        else
        {
            GpsData data = gpsModule.data();
            gpsScpiParser.serial()->print(data.speed_k, 1);
            gpsScpiParser.serial()->print(',');
            gpsScpiParser.serial()->print(data.track, 1);
            gpsScpiParser.serial()->print(',');
            gpsScpiParser.serial()->print(data.var, 1);
            gpsScpiParser.serial()->print(',');
            gpsScpiParser.serial()->println(status);
        }
    }
    else
    {
        gpsScpiParser.printStatus(status);
    }
    return status;
}

/**
 * @brief :GPS:QUAL? command implementation.
 * @param prmStr Command parameters (if any) passed as a string.
 * @param query Command is a query if true.
 * @return Command execution status.
 */
static gps_scpi_status_t gpsQualFn(char* prmStr, bool query)
{
    gps_scpi_status_t status = GPS_SCPI_CMD_ERR;
    if (query)
    {
        status = getStatCond();
        if (status < 0)
        {
            gpsScpiParser.printStatus(status);
        }
        else
        {
            GpsData data = gpsModule.data();
            gpsScpiParser.serial()->print(data.numsats);
            gpsScpiParser.serial()->print(',');
            gpsScpiParser.serial()->print(data.qual);
            gpsScpiParser.serial()->print(',');
            gpsScpiParser.serial()->print(data.hdop, 2);
            gpsScpiParser.serial()->print(',');
            gpsScpiParser.serial()->println(status);
        }
    }
    else
    {
        gpsScpiParser.printStatus(status);
    }
    return status;
}

/**
 * @brief :GPS:TIME? command implementation.
 * @param prmStr Command parameters (if any) passed as a string.
 * @param query Command is a query if true.
 * @return Command execution status.
 */
static gps_scpi_status_t gpsTimeFn(char* prmStr, bool query)
{
    gps_scpi_status_t status = GPS_SCPI_CMD_ERR;
    if (query)
    {
        status = getStatCond();
        if (status < 0)
        {
            gpsScpiParser.printStatus(status);
        }
        else
        {
            // Try to get a millisecond count that avoids
            // the 1-second transitions, and also provides
            // the host device time to take action before
            // the next second rollover.
            uint16_t millis = TickZero.millis();
            if ((millis < 20) || (millis > 400))
            {
                delay(1050 - millis);
            }
            
            GpsData data = gpsModule.data();
            gpsScpiParser.serial()->print(data.year);
            gpsScpiParser.serial()->print(',');
            gpsScpiParser.serial()->print(data.month);
            gpsScpiParser.serial()->print(',');
            gpsScpiParser.serial()->print(data.day);
            gpsScpiParser.serial()->print(',');
            gpsScpiParser.serial()->print(data.hour);
            gpsScpiParser.serial()->print(',');
            gpsScpiParser.serial()->print(data.minute);
            gpsScpiParser.serial()->print(',');
            gpsScpiParser.serial()->print(data.second);
            gpsScpiParser.serial()->print(',');
            gpsScpiParser.serial()->print(TickZero.millis());
            gpsScpiParser.serial()->print(',');
            gpsScpiParser.serial()->println(status);
        }
    }
    else
    {
        gpsScpiParser.printStatus(GPS_SCPI_CMD_ERR);
    }
    return status;
}

/**
 * @brief :STAT:COND? command implementation.
 * @param prmStr Command parameters (if any) passed as a string.
 * @param query Command is a query if true.
 * @return Command execution status.
 */
static gps_scpi_status_t statCondFn(char* prmStr, bool query)
{
    gps_scpi_status_t status = GPS_SCPI_CMD_ERR;
    if (query)
    {
        status = getStatCond();
    }
    gpsScpiParser.printStatus(status);
    return status;
}

/**
 * @brief :SYST:ECHO command implementation.
 * @param prmStr Command parameters (if any) passed as a string.
 * @param query Command is a query if true.
 * @return Command execution status.
 */
static gps_scpi_status_t systEchoFn(char* prmStr, bool query)
{
    gps_scpi_status_t status = GPS_SCPI_CMD_ERR;
    
    // Must supply a parameter.
    // Zero = disable, non-zero = enable
    char* pStr = getParamStart(prmStr);
    if ((!query) && (strlen(pStr) > 0))
    {
        bool enable = (atoi(pStr) != 0);
        gpsScpiParser.cmdEcho(enable);
        status = GPS_SCPI_OPC;
    }
    gpsScpiParser.printStatus(status);
    return status;
}

/**
 * @brief :SYST:LEDS command implementation.
 * @param prmStr Command parameters (if any) passed as a string.
 * @param query Command is a query if true.
 * @return Command execution status.
 */
static gps_scpi_status_t systLedsFn(char* prmStr, bool query)
{
    gps_scpi_status_t status = GPS_SCPI_CMD_ERR;
    
    // Must supply a parameter.
    // Zero = disable, non-zero = enable
    char* pStr = getParamStart(prmStr);
    if ((!query) && (strlen(pStr) > 0))
    {
        bool enable = (atoi(pStr) != 0);
        GPS_LED.enable(enable);
        status = GPS_SCPI_OPC;
    }
    gpsScpiParser.printStatus(status);
    return status;
}

/**
 * @brief :SYST:NMEA command implementation.
 * @param prmStr Command parameters (if any) passed as a string.
 * @param query Command is a query if true.
 * @return Command execution status.
 */
static gps_scpi_status_t systNmeaFn(char* prmStr, bool query)
{
    gps_scpi_status_t status = GPS_SCPI_CMD_ERR;
    
    // Must supply a parameter.
    // Zero = disable, non-zero = enable
    char* pStr = getParamStart(prmStr);
    if ((!query) && (strlen(pStr) > 0))
    {
        bool enable = (atoi(pStr) != 0);
        gpsModule.printNmea(enable);
        status = GPS_SCPI_OPC;
    }
    gpsScpiParser.printStatus(status);
    return status;
}

/**
 * @brief Get the GPS FIX/PPS status and the RTC status.
 * @return The GPS and RTC status.
 */
static gps_scpi_status_t getStatCond()
{
    // Get the GPS FIX/PPS status and the RTC status.
    gps_scpi_status_t status = GPS_SCPI_NONE;
    bool fix = gpsModule.hasFix();
    switch (g_timeSyncMaster)
    {
    case SYNC_NONE:
    default:
        status = (fix) ? GPS_SCPI_FIXONLY : GPS_SCPI_NOSYNC;
        break;
      
    case SYNC_GPS:
        status = (fix) ? GPS_SCPI_FIXPPS : GPS_SCPI_PPSONLY;
        break;
        
    case SYNC_RTC:
        status = (fix) ? GPS_SCPI_FIXRTC : GPS_SCPI_RTCONLY;
        break;
    }
    return status;
}

/**
 * @brief Recursively print the mnemonics from the parser tables, used by listFn().
 * @param table The parser table containing the mnemonics to print.
 * @param level The mnemonic level.  Successive levels are indented two spaces.
 * The top level is level 0.
 * @param sep The mnemonic character separator, either '*' or ':'.
 */
static void printMnemonic(const gps_scpi_parser_entry_t* table, int level, char sep)
{
    int index  = 0;
    int spaces = level << 1;
    
    const char* mn_str = table[index].mnemonic;
    const gps_scpi_parser_entry_t* nextTable = table[index].nextTable;
    int mn_len = strlen(mn_str);
    
    while (mn_len > 0)
    {
        // Print leading spaces, then the separator, then the mnemonic.
        for (int i = 0; i < spaces; i++) gpsScpiParser.serial()->print(' ');
        gpsScpiParser.serial()->print(sep);
        gpsScpiParser.serial()->println(mn_str);
        
        if (nextTable)
        {
            // Recursively print the next mnemonic table.
            printMnemonic(nextTable, level+1, sep);
        }
        
        // Get the next table entry.
        index++;
        mn_str = table[index].mnemonic;
        nextTable = table[index].nextTable;
        mn_len = strlen(mn_str);
    }  
}

/*****************************************************************************
 * Interrupt service routines.
 *****************************************************************************/
 
 
// End of file.
