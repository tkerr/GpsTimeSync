/******************************************************************************
 * GpsTimeSync.h
 * Copyright (c) 2020 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Arduino-based GPS time sync application intended for amateur radio portable operation.
 *
 * See Mainpage.h for details.
 */

#ifndef _GPS_TIME_SYNC_H
#define _GPS_TIME_SYNC_H

/******************************************************************************
 * System include files.
 ******************************************************************************/


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "ISerial.h"
#include "GpsData.h"
#include "GPS_MTK3339.h"


/******************************************************************************
 * Public definitions.
 ******************************************************************************/

// Uncomment if using status LEDs.
//#define HAS_GPS_LED

// Uncomment if using Adafruit FeatherWing 128x32 OLED display.
#define HAS_OLED_DISPLAY

// Serial baud rates.
#define MONITOR_SERIAL_BAUD (115200)  //!< Arduino serial monitor baud rate
#define GPS_SERIAL_BAUD (9600)        //!< GPS module baud rate

// GPS module pins
#define GPS_RESET_PIN 10 //!< GPS reset input, active low
#define GPS_FIX_PIN   11 //!< GPS fix external interrupt pin
#define GPS_1PPS_PIN  12 //!< GPS 1 pulse-per-second (1PPS) external interrupt pin


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

/**
 * @brief The global GPS module object.
 */
extern GPS_MTK3339 gpsModule;

/**
 * @brief The global serial monitor object.
 * Used to send messages to the Arduino user console.
 */
extern ISerial monitor;

 
/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/


#endif // _GPS_TIME_SYNC_H
